const name = 'Иван-Царевич';
const element = '<h1>Здравствуй, {name}!</h1>';

ReactDOM.render(
  element,
  document.getElementById('root')
);